assert filter:(->e e > 3):[3 5 1 2 10 2] == [5 10]
assert filter:(->[k v] v):{a=true b=false c=5 d=nil} == [["a" true] ["c" 5]]

# assert is_err:(filter:(->v v):[1 2 err:"example" 4 5])
