assert 1 || 2 == 1
assert 0 && 2 == 2
assert "" && "foo" == "foo"
assert "foo" && 3 == 3
assert false && 3 == false
assert nil || false || {} || 4 == {}

assert nil && nil.error == nil
