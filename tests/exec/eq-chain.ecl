# One day I would like to support chaining like in python.
# assert 1 == 1 == 1
# assert 1 < 2 <= 3
# However until that is supported make it a syntax error. If people really want to compare the result of the comparison they can put parens to make it explicit.
assert (1 < 2) == true

syntax-error = nil == nil == nil
