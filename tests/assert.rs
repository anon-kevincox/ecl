use erased_serde::Serialize;

mod utils;

fn main() {
	std::env::set_var("SET_VAR", "set to this value");
	std::env::remove_var("UNSET_VAR");

	utils::test_dir("assert", "ecl", |path| {
		match ecl::eval_file(&path.to_string_lossy()).eval() {
			Ok(v) => {
				v.erased_serialize(&mut serde_json::Serializer::new(std::io::sink())).unwrap();
			}
			Err(e) => panic!("Got error: {:?}", e),
		}
	});
}
