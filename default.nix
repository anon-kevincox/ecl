with import <nixpkgs> {
	overlays = [
		(import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz))
	];
}; let
in rec {
	ecl = rustPlatform.buildRustPackage.override { rustc = latest.rustChannels.nightly.rust; } {
		name = "ecl";
		cargoSha256 = null;
		# buildInputs = [ pkgconfig ];
		src = builtins.filterSource (name: type:
			(lib.hasPrefix (toString ./src) name) ||
			(lib.hasPrefix (toString ./tests) name) ||
			(lib.hasPrefix (toString ./Cargo) name)) ./.;
		# cargoBuildFlags = ["--bin=tiles"];
		doCheck = false;
	};
}
