use erased_serde;

#[derive(Debug)]
pub struct Null;

pub fn get() -> crate::Val {
	crate::Val::new_inline(crate::Inline::Null)
}

impl crate::Value for Null {
	fn type_str(&self) -> &'static str { "null" }

	fn serialize(&self,  _: &mut Vec<*const dyn crate::Value>, s: &mut dyn erased_serde::Serializer)
		-> Result<(),erased_serde::Error> {
		s.erased_serialize_none()
	}

	fn to_string(&self) -> crate::Val {
		crate::Val::new_atomic("null".to_owned())
	}

	fn to_bool(&self) -> crate::Val {
		crate::bool::get(false)
	}
}

impl crate::SameOps for Null {
	fn cmp(&self, _that: &Self) -> Result<std::cmp::Ordering,crate::Val> {
		Ok(std::cmp::Ordering::Equal)
	}
}
